#ifndef STEPS_TOTAL 
  #define STEPS_TOTAL  2048 
#endif
#ifndef STEPS_UP 
  #define STEPS_UP    1
#endif
#ifndef STEPS_DOWN 
  #define STEPS_DOWN  -1
#endif

#ifndef IN1 
  #define IN1  2
#endif
#ifndef IN2 
  #define IN2  3
#endif
#ifndef IN3 
  #define IN3  4
#endif
#ifndef IN4 
  #define IN4  5
#endif

#ifndef SPEED_BASE 
  #define SPEED_BASE  8
#endif
#ifndef SPEED_MAX 
  #define SPEED_MAX   12
#endif

#ifndef SPLIT_BASE 
  #define SPLIT_BASE  8
#endif
#ifndef SPLIT_MAX 
  #define SPLIT_MAX   10
#endif

#ifndef BTTN_MODE 
  #define BTTN_MODE       8
#endif
#ifndef BTTN_DIRECTION 
  #define BTTN_DIRECTION  9
#endif
#ifndef BTTN_SPEED 
  #define BTTN_SPEED      10
#endif

#ifndef LED_PIN 
  #define LED_PIN  11
#endif
