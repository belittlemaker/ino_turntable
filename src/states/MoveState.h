#ifndef MoveState_h
#define MoveState_h

    #include <Arduino.h>
    #include "../devices/Devices.h"
    #include "State.h"

    class MoveState : public State 
    {
        public:
            MoveState(Devices *devices);
            void input();
            void output();
        private:
            int rotation_speed;
            int steps;
    };

#endif // MoveState_h
