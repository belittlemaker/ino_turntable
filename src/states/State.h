#ifndef State_h
#define State_h

    #include <Arduino.h>
    #include "../devices/Devices.h"
    
    class State
    {      
        public:
            State(Devices *devices);
            virtual void input();
            virtual void output();
            void set_next_mode_state(State *state);
            State *transition();
        protected:
            Devices *devices;
            State *next_mode_state;
    };

#endif // State_h
