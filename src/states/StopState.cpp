#include "Defines.h"
#include "StopState.h"

StopState::StopState(Devices *devices) : State(devices) {

}

void StopState::input()
{

}

void StopState::output()
{
    this->devices->led->switch_off();
    this->devices->motor->stop_motor();
}
