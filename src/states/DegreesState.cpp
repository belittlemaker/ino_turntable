#include "Defines.h"
#include "DegreesState.h"

DegreesState::DegreesState(Devices *devices) : State(devices) {
    this->split = SPLIT_BASE;
    this->step = false;
}

void DegreesState::input()
{
    if (this->devices->bttn_opt1->pushed()) {
        this->step = true;
    }
    
    if (this->devices->bttn_opt2->pushed()) {
        this->split = this->split < SPLIT_MAX ? this->split + 1 : SPLIT_BASE;
    }
}

void DegreesState::output()
{
    this->devices->led->switch_on();
    this->devices->motor->stop_motor();

    if (this->step) {
        long degrees = 360 / this->split;

        this->devices->motor->set_rotation_speed(SPEED_BASE);
        this->devices->motor->move_degrees(degrees);
    }

    this->step = false;
}
