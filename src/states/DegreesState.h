#ifndef DegreesState_h
#define DegreesState_h

    #include <Arduino.h>
    #include "../devices/Devices.h"
    #include "State.h"

    class DegreesState : public State 
    {
        public:
            DegreesState(Devices *devices);
            void input();
            void output();
        private:
            bool step;
            int split;
    };

#endif // DegreesState_h
