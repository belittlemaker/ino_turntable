#ifndef StopState_h
#define StopState_h

    #include <Arduino.h>
    #include "../devices/Devices.h"
    #include "State.h"

    class StopState : public State 
    {
        public:
            StopState(Devices *devices);
            void input();
            void output();
    };

#endif // StopState_h
