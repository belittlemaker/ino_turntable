#include "Defines.h"
#include "MoveState.h"

MoveState::MoveState(Devices *devices) : State(devices) {
    this->rotation_speed = SPEED_BASE;
    this->steps = STEPS_UP;
}

void MoveState::input()
{
    if (this->devices->bttn_opt1->pushed()) {
        switch (this->steps) {
            case STEPS_DOWN:
                this->steps = STEPS_UP;
                break;
            case STEPS_UP: 
                this->steps = STEPS_DOWN;
                break;
        }
    }
    
    if (this->devices->bttn_opt2->pushed()) {
        this->rotation_speed = this->rotation_speed < SPEED_MAX ? this->rotation_speed + 2 : SPEED_BASE;
    }
}

void MoveState::output()
{
    this->devices->led->switch_on();
    this->devices->motor->set_rotation_speed(this->rotation_speed);
    this->devices->motor->move_steps(this->steps);
}
