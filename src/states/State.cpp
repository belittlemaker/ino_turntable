#include "State.h"

State::State(Devices *devices) {
    this->devices = devices;
}

void State::set_next_mode_state(State *state)
{
    this->next_mode_state = state;
}


State *State::transition() 
{
    if (this->devices->bttn_mode->pushed()) {
        return this->next_mode_state;
    }
    return this;
}
