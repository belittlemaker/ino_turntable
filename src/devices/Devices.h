#ifndef Devices_h
#define Devices_h

    #include <Arduino.h>
    #include "Button.h"
    #include "Led.h"
    #include "StepperMotor.h"
    
    class Devices
    {      
        public:
            Devices(
                StepperMotor *motor,
                Led *led, 
                Button *bttn_mode, 
                Button *bttn_opt1,
                Button *bttn_opt2
            );
            StepperMotor *motor;
            Led *led;
            Button *bttn_mode;
            Button *bttn_opt1;
            Button *bttn_opt2;
    };

#endif // Devices_h
