#include "Button.h"

Button::Button(int pin)
{
    this->pin = pin;
    this->state = LOW;
}

bool Button::pushed()
{
    int stateNew = this->read_state();

    bool pushed = stateNew == HIGH && this->state == LOW;
    
    this->state = stateNew;

    return pushed;
}

int Button::read_state()
{
    return digitalRead(this->pin);
}
