#include "Led.h"

Led::Led(int pin)
{
    this->pin = pin;
    
    pinMode(this->pin, OUTPUT);

    this->switch_off();
}

void Led::switch_on()
{
    digitalWrite(this->pin, HIGH);
}

void Led::switch_off()
{
    digitalWrite(this->pin, LOW);
}
