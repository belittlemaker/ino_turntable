#include "Devices.h"

Devices::Devices(
    StepperMotor *motor, 
    Led *led,
    Button *bttn_mode, 
    Button *bttn_opt1, 
    Button *bttn_opt2
) {
    this->motor = motor;
    this->led = led;
    this->bttn_mode = bttn_mode;
    this->bttn_opt1 = bttn_opt1;
    this->bttn_opt2 = bttn_opt2;
}
