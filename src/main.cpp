#include <Arduino.h>

#include "Defines.h"

#include "devices/Button.h"
#include "devices/Devices.h"
#include "devices/Led.h"
#include "devices/StepperMotor.h"

#include "states/DegreesState.h"
#include "states/MoveState.h"
#include "states/StopState.h"
#include "states/State.h"

Button *bttn_mode;
Button *bttn_opt1;
Button *bttn_opt2;
Devices *devices;
Led *led;
StepperMotor *motor;

DegreesState *degrees_state;
MoveState *move_state;
StopState *stop_state;
State *current_state;

void setup()
{
    bttn_mode = new Button(BTTN_MODE);
    bttn_opt1 = new Button(BTTN_DIRECTION);
    bttn_opt2 = new Button(BTTN_SPEED);

    led = new Led(LED_PIN);
    
    motor = new StepperMotor(STEPS_TOTAL, IN1, IN3, IN2, IN4);
    motor->set_rotation_speed(SPEED_BASE);

    devices = new Devices(motor, led, bttn_mode, bttn_opt1, bttn_opt2);

    degrees_state = new DegreesState(devices);
    move_state = new MoveState(devices);
    stop_state = new StopState(devices);

    degrees_state->set_next_mode_state(stop_state);
    move_state->set_next_mode_state(degrees_state);
    stop_state->set_next_mode_state(move_state);

    current_state = stop_state;
}

void loop()
{
    current_state->input();
    current_state->output();
    current_state = current_state->transition();
}
