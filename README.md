# ino_turntable
Arduino controller for a turntable machine

Project
-------
- Build in Platformio for Visual Studio Code
  - https://platformio.org/install/ide?install=vscode
  - Physical components:
    - x1 Arduino Nano 
    - x1 Leds (green)
    - x3 Switches/buttons
    - x4 Resistors:
      - x1 220 OHM (Led)
      - x3 10 KILOHM (Switches)
    - x1 StepMotor 28BYJ-48 5V
    - x1 ULN2003 Driver

Steps before
------------
Review the configuration files & choose your own configuration:
- StepMotor & Buttons & Led pins
  - Defines.h

Functionality
-------------
Switches:
- Switch (Mode):
  - Change between modes
    - Stop
    - Infinite movement
    - Movement steps by degrees
- Switch (Opt1):
  - Infinite Mode:
    - Change direction
  - Degrees Mode:
    - Step degrees
- Switch (Opt2):
  - Infinite Mode:
    - Increase speed
  - Degrees Mode:
    - Increase number of steps

Led:
  - Turn On when not in Stop Mode
